# Description
New boats, oil press, fish extract, crane and more to help you explore this giant world.</br>

Now you can use the fish you catch to extract the Fish Extract using the Oil Press, the fish extract gives you strength to carry more weight (450kg).
Gather ten fishs and create the Fish Basket that is used in the oil press.</br>
Use the pier, crane, fishing net, fish stand, totem poles, etc... to decorate your harbor.</br>

![enter image description here](https://i.ibb.co/XDZdzht/mosaico.jpg)

## Support

Did you like my Mods? If you want to support me click on the image link below:</br>

<a href="https://www.paypal.com/donate/?hosted_button_id=ZRQZGAVYEUBX2"><img src="https://i.ibb.co/kJqcqkg/Pay-Pal-Donate.png" /></a>

*Any donation help me to continue creating and updating mods. Thank you very much for your support.**

### GITS FOR DONORS
`Donors get extra freebie content. For example, the boat in the photo below, exclusively for donors.`

![OdinShip Extra Content](https://i.ibb.co/QKtn6Lz/Extra-Donors-Gift.jpg)

`After your donation, contact me through the discord links below to receive your gift.`


# Thanks

My sincere thanks to the entire OdinPlus team -  <i>@Azummat - @Blaxxun's</i> and @Lunchbox for the 3D models and for appreciating my work.

A special thanks to my Professor <i>@GraveBear</i>, without his teachings and his encouragement I would never have gotten it.

I want to thank Archaeologist and 3D artist Alberto Foglia who donated one of his most beautiful models (Viking Longship - Skuldelev 2) to be used in our mod and further enrich our Viking world.Few people in the world have such a kind heart. Follow his wonderful work at the link below.

https://sketchfab.com/OpusPoly and https://www.artstation.com/albertofoglia


# Manual Install

>Mod is required on Server for Config Sync (this is still in development). 

>Download the latest copy of Bepinex per author's instructions.

>Place the OdinShip.dll inside of the "Bepinex\plugins\" folder.

<p align="center"><b>For Questions or Comments find Marlthon in the Bagual Server Discord or Odin Plus Team on Discord:</b></p>

<p align="center"><b><a href="https://discord.gg/zNpsGQW2ZF">BagualServer</a> or <a href="https://discord.gg/mbkPcvu9ax">OdinPlus</a></b></p>


# Change log
v0.0.1
 - First version released, First version released, server sync active. (Will override player config if installed to server)
 
v0.0.2
 - Added Vulkan support
 
 v0.0.3
 - Added Big Cargo Ship
 
 v0.0.4
 - Fixed bugs on vulkan, textures, material return and added Decorative Cargo Ship.
 
  v0.0.5
 - Fixed sail bug and other small fixes.
 
  v0.0.6
 - Added WarShip and other small fixes.
 
  v0.0.7
 - Fixed Vulkan textures
 
  v0.0.8
 - Added Fishing Boat, Transport Ship and fixed some bugs.
 
  v0.0.9
 - Fixed material destruction and return errors in FishingBoat and WarShip, added Russian and Portuguese BR translations.
 
   v0.1.0
 - Adding decorative items for pier, pulleys, crane, totems, Fish Extract and Oil Press. Made minor corrections and additions to the boats.
 
   v0.1.1
 - Added Skuldelev Warship.
 - Added Destruction effect to ships.
 - Replaced the Fishing Boat 3D Model.
 - Added Pier Crane and Construction Ship.
 - Effect improvements and other small fixes and additions.
 - Changed duration time and carrying capacity of Fish Extract.

   v0.1.2
 - Updated ServerSync for crossplay.
 
   v0.1.3
 - Serversync v1.13 Update to Fix Valheim Patch 0.211.11 (10/28/2022)
 - Added translations. (Portuguese, Chinese, Danish, French, German, Greek, Icelandic, Italian, Russian and Spanish)
 - Added again Fishing Canoe (On request)
 
   v0.1.4
 - Fixed bug in english language
 
   v0.1.5
 - Fixed little bugs, added rats in CargoShip and Viking-themed Shields in Skuldelev Warship.
 
   v0.1.6
 - Added turrets to War Ship, War Ship Skuldelev and Cargo Caravel (Plus)
 - Boats cannot now be dismantled using the hammer.
   
   v0.1.7
 - Fixed HarmonyPatch Bug.
 
   v0.1.8
 - Encryption removal.
 
   v0.1.9
 - Fixed onboard deck sounds.
 
   v0.2.0
 - Reduced the volume of deck sounds.
 - Fixed effect bugs when destroying ships.
 
   v0.2.1
 - Improvements to water effects.
 
   v0.2.2
 - Fixed issue with fish oil extractor.